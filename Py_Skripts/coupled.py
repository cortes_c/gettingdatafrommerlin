#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 19 14:14:30 2018

@author: cristopher
"""

import numpy as np
import matplotlib.pyplot as plt 

def coscos(x,wp,wm):
    
    y = np.cos((wp-wm)*x/2)*np.cos((wp+wm)*x/2) 
    return y

def sincos(x,wp,wm):
    
    y = np.sin((wp-wm)*x/2)*np.cos((wp+wm)*x/2) 
    return y

def main():
    
    s = np.linspace(0,4*np.pi,100000)
    wp = 100
    wm = 1
    
    x = coscos(s,wp,wm)/2 + sincos(s,wp,wm)/2
    
    plt.plot(s,x,label="x",linestyle="",marker=".")
    plt.xlabel("s")
    plt.ylabel("x(s)")

def main1():
    
    s = np.linspace(0,4*np.pi,10000)
    w = 10
    D =0.1
    x = np.sinh(D*s)*np.cos(w*s) + (np.sinh(D*s) + np.cosh(D*s))*np.sin(w*s)
    l = (np.cosh(D*s) + np.sinh(D*s))*np.cos(w*s) + np.sinh(D*s)*np.sin(w*s)
    plt.plot(s,x,label="x",linestyle="",marker=".")
    plt.plot(s,l,label="l",linestyle="",marker=".")
    plt.xlabel("s")
    plt.ylabel("x(s)")
    plt.legend(loc=0)
    plt.title("Diverging equations of motion",fontsize =20)
    
main1()
    
    