#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 12:32:13 2018

@author: cristopher
"""
# %matplotlib auto for interactive console

import numpy as np
import matplotlib.pyplot as plt

twissOPAL_15nA =  "/home/cristopher/Documents/PSI/gettingdatafrommerlin/twiss15nA.txt"
twissOPAL_1_5muA = "/home/cristopher/Documents/PSI/gettingdatafrommerlin/twiss1_5muA.txt"
twissOPAL_2_2mA ="/home/cristopher/Documents/PSI/gettingdatafrommerlin/twiss2_2mA1.txt"

mat15nA = [-0.01708,6.738,1.472,0.005206,6.922,1.426, -2.737e-05,1050,0.009441]
mat2_2mA =[-0.02728,13.28, 1.474,0.005708,7.071,1.396,-0.004438,11.01,1.777]
mat1_5muA = [-0.02002,8.558,1.472,0.005206,6.922,1.426,-0.001529,22.86,0.551]
def twiss_darstellen(file, erwartung):
    
    twiss = np.loadtxt(file,unpack ="True")    
    
    direction = {0:'x',1:'y',2:'z'}
    
    for i in range(3):
    
        alpha = twiss[i*3]
        beta = twiss[i*3 + 1]
        gamma = twiss[i*3 + 2]
        
        a = erwartung[i*3]
        b = erwartung[i*3 + 1]
        g = erwartung[i*3 + 2]
        
        plt.figure(i)
        
        plt.plot(alpha,label=r"$\epsilon \alpha_{}$".format(direction[i]),marker=".")
        plt.plot(beta,label=r"$\epsilon \beta_{}$".format(direction[i]),marker=".")
        plt.plot(gamma,label=r"$\epsilon \gamma_{}$".format(direction[i]),marker=".")
        plt.axhline(a,color="b",label = r"$\epsilon \alpha$")
        plt.axhline(b,color="orange",label = r"$\epsilon \beta$")
        plt.axhline(g,color="g",label = r"$\epsilon \gamma$")
        
        plt.legend(loc=0)
        plt.xlabel("Iteration")
        plt.title("{}-Direction".format(direction[i]))
        plt.xlim(0,len(alpha))
        plt.ylim(-3*max(erwartung),3*max(erwartung))
        
        plt.show()
        
        #plt.savefig("Twiss{}".format(i))
    
        
twiss_darstellen(twissOPAL_2_2mA,mat2_2mA)
