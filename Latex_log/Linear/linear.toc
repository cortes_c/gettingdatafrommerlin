\contentsline {chapter}{\numberline {1}Theoretical background}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Cyclotron}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Classical cyclotron}{1}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Isochroncyclotron}{2}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Coordinate System}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Equations of motion}{4}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Stability criteria and eigenvalue order}{6}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Transfer Matrix}{9}{subsection.1.3.2}
\contentsline {section}{\numberline {1.4}Twiss Parameters}{10}{section.1.4}
\contentsline {section}{\numberline {1.5}Second moment matrix}{10}{section.1.5}
\contentsline {section}{\numberline {1.6}Matched Distribution}{11}{section.1.6}
\contentsline {section}{\numberline {1.7}Space charge and $\sigma $-Matrix dependence}{12}{section.1.7}
\contentsline {chapter}{\numberline {2}Implementation and results}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}Implementation}{13}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Initialization}{13}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Update scheme}{14}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Error handling and implementation remarks}{15}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Results}{15}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Injector II}{16}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}PSI-Ring}{19}{subsection.2.2.2}
